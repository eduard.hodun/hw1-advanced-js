class Employee {

    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get userName() {
      return this.name;
    }
    set userName(name) {
        this.name = name;
    }

    get userAge() {
        return this.age;
    }
    set userAge(age){
        this.age = age;
    }

    get userSalary(){
        return this.salary;
    }
    set userSalary(salary){
         this.salary = salary;
    }


}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang  = lang;
    }

    get userSalary(){
        return this.salary * 3;
    }
}

let user1 = new Programmer('Ed', 17, 2500, "English");
let user2 = new Programmer('Ivan', 25, 20500, "JS");
let user3 = new Programmer('Avel', 35, 30500, "C#");

console.log(user1);
console.log(user1.userName);
console.log(user1.userAge);
console.log(user1.userSalary);

console.log(user2);
console.log(user2.userName);
console.log(user2.userAge);
console.log(user2.userSalary);

console.log(user3);
console.log(user3.userName);
console.log(user3.userAge);
console.log(user3.userSalary);
